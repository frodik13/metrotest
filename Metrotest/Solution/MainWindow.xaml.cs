﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Solution
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool flag = false;
        private Generation generation;
        private Display display;

        public MainWindow()
        {
            InitializeComponent();
            generation = new Generation();
            display = new Display(generation, FormPlot);
        }

        private async void StartBut_Click(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(Amplitude.Text, out double amplitude) && double.TryParse(Frequency.Text, out double frequensy))
            {
                if (!flag)
                    flag = true;

                StartBut.IsEnabled = false;
                StopButton.IsEnabled = true;

                generation.Amplitude = amplitude;
                generation.Frequency = frequensy;
                display.AddSignal();
                
                while (flag)
                {
                    await Task.Delay(100);
                    await this.Dispatcher.BeginInvoke(() =>
                    {
                        generation.UpdateValues();
                    }, DispatcherPriority.Background);
                    await this.Dispatcher.BeginInvoke(() =>
                    {
                        display.SetAxisLimitsXY();
                        display.UpdateDisplay();
                    }, DispatcherPriority.Normal);
                }
            }
            else
            {
                MessageBox.Show("Invalid data");
            }
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            if (flag)
                flag = false;

            StartBut.IsEnabled = true;
            StopButton.IsEnabled = false;
        }
    }
}
