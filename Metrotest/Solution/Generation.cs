﻿using System;
using System.Diagnostics;

namespace Solution
{
    public class Generation
    {
        public double Amplitude { get; set; }
        public double Frequency { get; set; }

        public double[]? Values { get; set; }

        private readonly Stopwatch? Stopwatch;

        

        public void UpdateValues()
        {
            if (Stopwatch != null)
            {
                var phase = Stopwatch.Elapsed.TotalSeconds;
                if (Values != null)
                {
                    for (int i = 0; i < Values.Length; i++)
                        Values[i] = Amplitude * Math.Sin(i * 2 * Math.PI * Frequency / phase);
                }
            }
        }
        
        public Generation()
        {
            Stopwatch = Stopwatch.StartNew();
            Values = new double[200];
        }
    }
}
