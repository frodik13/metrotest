﻿using ScottPlot;
using ScottPlot.Plottable;

namespace Solution
{
    public class Display
    {
        #region Поля и свойства

        public Generation? Generation { get; set; }

        public WpfPlot? Plot { get; set; }

        private SignalPlot? signal;

        #endregion

        #region Методы

        public void UpdateDisplay()
        {
            Plot?.Refresh(true);
        }

        public void SetAxisLimitsXY()
        {
            if (Plot != null)
            {
                if (Generation?.Values != null)
                {
                    var axisLimitY = Generation.Amplitude * .1;
                    Plot.Plot.SetAxisLimitsY(0 - Generation.Amplitude - axisLimitY, Generation.Amplitude + axisLimitY);
                    Plot.Plot.SetAxisLimitsX(0, Generation.Values.Length);
                }
            }
        }

        public void AddSignal()
        {
            if (Plot != null)
            {
                if (Generation?.Values != null)
                {
                    signal = Plot.Plot.AddSignal(Generation.Values);
                }
            }
        }

        #endregion

        #region Конструктор

        public Display(Generation? generation, WpfPlot? plot)
        {
            Generation = generation;
            Plot = plot;
            //Plot?.Plot.SetAxisLimitsX(0, 150);
        }

        #endregion
    }
}
